# ZTimer
ZTimer is a simple timer and chronometer made with the Qt framework.

Screenshot of ZTimer:

![alt text](https://raw.githubusercontent.com/SZinedine/ZTimer/master/screenshot.png "ZTimer screenshot")


